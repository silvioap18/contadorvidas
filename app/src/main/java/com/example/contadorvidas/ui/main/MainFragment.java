package com.example.contadorvidas.ui.main;

import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.contadorvidas.R;
import com.google.android.material.snackbar.Snackbar;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    private Button poissonUp1;
    private Button poissonDown1;
    private Button poissonDown2;
    private Guideline guideline3;
    private ImageButton downToUpLife;
    private ImageButton upToDownLife;
    private Button poissonUp2;
    private ImageButton upLife1;
    private ImageButton upLife2;
    private ImageButton loseLife1;
    private ImageButton loseLife2;
    private TextView textLife1;
    private TextView textLife2;

    private int life1;
    private int life2;
    private int poison1;
    private int poison2;

    public View view;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("life1", life1);
        outState.putInt("life2", life2);
        outState.putInt("poison1", poison1);
        outState.putInt("poison2", poison2);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.main_fragment, container, false);

        poissonUp1 = view.findViewById(R.id.poissonUp1);
        poissonUp2 = view.findViewById(R.id.poissonUp2);
        poissonDown1 = view.findViewById(R.id.poissonDown1);
        poissonDown2 = view.findViewById(R.id.poissonDown2);
        downToUpLife = view.findViewById(R.id.downToUpLife);
        upToDownLife = view.findViewById(R.id.upToDownLife);
        upLife1 = view.findViewById(R.id.upLife1);
        upLife2 = view.findViewById(R.id.upLife2);
        loseLife1 = view.findViewById(R.id.loseLife1);
        loseLife2 = view.findViewById(R.id.loseLife2);
        textLife1 = view.findViewById(R.id.textLife1);
        textLife2 = view.findViewById(R.id.textLife2);

        View.OnClickListener listener = view -> {
            switch (view.getId()) {
                case R.id.upLife1:
                    life1++;
                    break;

                case R.id.upLife2:
                    life2++;
                    break;

                case R.id.loseLife1:
                    if (comprobadorNumeroNegativo(life1)) {
                        life1--;
                    }
                    break;

                case R.id.loseLife2:
                    if (comprobadorNumeroNegativo(life2)) {
                        life2--;
                    }
                    break;

                case R.id.poissonUp1:
                    poison1++;
                    break;

                case R.id.poissonDown1:
                    if (comprobadorNumeroNegativo(poison1)) {
                        poison1--;
                    }
                    break;

                case R.id.poissonUp2:
                    poison2++;
                    break;

                case R.id.poissonDown2:
                    if (comprobadorNumeroNegativo(poison2)) {
                        poison2--;
                    }
                    break;

                case R.id.upToDownLife:
                    if (comprobadorNumeroNegativo(life1)) {
                        life1--;
                        life2++;
                    }
                    break;

                case R.id.downToUpLife:
                    if (comprobadorNumeroNegativo(life2)) {
                        life1++;
                        life2--;
                    }
                    break;

            }
            updateViews();
        };


        reset();

        poissonUp1.setOnClickListener(listener);
        poissonDown1.setOnClickListener(listener);
        poissonDown2.setOnClickListener(listener);
        downToUpLife.setOnClickListener(listener);
        upToDownLife.setOnClickListener(listener);
        poissonUp2.setOnClickListener(listener);
        upLife1.setOnClickListener(listener);
        upLife2.setOnClickListener(listener);
        loseLife1.setOnClickListener(listener);
        loseLife2.setOnClickListener(listener);
        textLife1.setOnClickListener(listener);
        textLife2.setOnClickListener(listener);

        if (savedInstanceState != null) {
            life1 = savedInstanceState.getInt("life1");
            life2 = savedInstanceState.getInt("life2");
            poison1 = savedInstanceState.getInt("poison1");
            poison2 = savedInstanceState.getInt("poison2");

            updateViews();
        }

        return view;
    }

    public boolean comprobadorNumeroNegativo(int numero) {
        return numero > 0;
    }

    public void reset() {
        life1 = 20;
        life2 = 20;
        poison1 = 0;
        poison2 = 0;
        updateViews();
    }

    private void updateViews() {
        textLife1.setText(String.format("%d/%d", life1, poison1));
        textLife2.setText(String.format("%d/%d", life2, poison2));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_reset, menu);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.btnReset) {
            reset();
            Snackbar.make(view, "New Game!", Snackbar.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}